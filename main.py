import requests
import json
import time
from obscene_words_filter.default import get_default_filter
auth = 'http://127.0.0.1:8000/'
route = 'https://digitalhack20190721123423.azurewebsites.net/' #'http://172.20.10.2:51866/'
sleep_time = 60 #seconds


class Spider:
    def __init__(self, auth, route, period, stop_file=None):
        self.auth = auth
        self.route = route
        self.period = period
        self.filter = f = get_default_filter()
        self.token = ''
        self.stop_file = stop_file
        self.stop_words = []
        self.user = {
                "login": "00000000000",
                "password": "password"
            }
        self.load_stop_words()

    def get_comments(self):
        try:
            s = requests.Session()
            s.headers.update({'content-type': 'application/json; charset=utf-8'})
            rsp = s.post(auth + 'login', data=json.dumps(self.user))
            self.token = rsp.json()['access_token']
            s.headers.update({'content-type': 'application/json; charset=utf-8',
                              "Authorization": "Bearer " + self.token})
            # print(token)
            rsp = s.get(route + 'api/Comment/GetNotValidated')
            # print(rsp.text)
            return rsp.json()
        except Exception as e:
            return []

    def load_stop_words(self):
        if not self.stop_file:
            return
        with open(self.stop_file, 'r') as file:
            self.stop_words = file.readlines()

    def validate_comments(self, comments, bannedIds, validatedIds):
        for comment in comments:
            if self.is_bad_comment(comment):
                bannedIds.append(comment['id'])
            validatedIds.append((comment['id']))

    def is_bad_comment(self, comment):
        tmp_str = comment['message'].split(' ')
        for i in tmp_str:
            if self.filter.is_word_bad(i):
                return True

        for i in tmp_str:
            if i in self.stop_words:
                return True
        return False

    def send_response(self, bannedIds, validatedIds):
        try:
            s = requests.Session()
            s.headers.update({'content-type': 'application/json; charset=utf-8'})
            rsp = s.post(auth + 'login', data=json.dumps(self.user))
            self.token = rsp.json()['access_token']
            s.headers.update({'content-type': 'application/json; charset=utf-8',
                              "Authorization": "Bearer " + self.token})
            # print(self.token)
            data = {
                'BannedIds': bannedIds, 'ValidatedIds': validatedIds
            }
            # print(data)
            rsp = s.post(route + '/api/Comment/SendValidationResult', data=json.dumps(data))
            # print(rsp.text)
            return rsp.json()
        except Exception as e:
            return []

    def run(self):
        while True:
            bannedIds = []
            validatedIds = []
            comments = self.get_comments()
            # print(comments)
            self.validate_comments(comments, bannedIds, validatedIds)
            # print(bannedIds, validatedIds)
            if validatedIds:
                self.send_response(bannedIds, validatedIds)
            time.sleep(self.period)


if __name__ == "__main__":
    p = Spider(auth, route,sleep_time)
    p.run()


